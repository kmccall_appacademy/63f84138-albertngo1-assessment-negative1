# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers(nums)
  minimum = nums.min
  maximum = nums.max

  missing_number_arr = []

  i = minimum

  until i == maximum

    missing_number_arr << i if !nums.include?(i)
    i += 1
  end

  missing_number_arr

end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)
  binary_length = binary.length

  binary_int = binary.chars.map do |digit|
    digit.to_i
  end
  answer = 0
  (binary_length-1).downto(0).each_with_index do |pow, idx|
    answer += binary_int[idx] * (2 ** pow)
  end
  answer

end

class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)

    answer = {}

    self.each do |key, value|

      answer[key] = value if prc.call(key, value)

    end
    answer
  end

end

class Hash

  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.

  def my_merge(hash, &prc)
    answer = self

    if block_given?
      hash.each do |k, v|
        if self.keys.include?(k)
          answer[k] = prc.call(k, answer[k], v)
        else
          answer[k] = v
        end
      end
    else
      hash.each do |k, v|
        answer[k] = v
      end
    end

    answer
  end

end

# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(n)
  lucas_array = [2, 1]

  if n < 0
    i = 0
    until i == n
      lucas_array.unshift(lucas_array[1] - lucas_array[0])
      i -= 1
    end
    return lucas_array.first
  elsif n > 1
    i = 1
    until i == n
      lucas_array.push(lucas_array[-1]+lucas_array[-2])
      i += 1
    end
  end
  lucas_array[n]


end

# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.

def longest_palindrome(string)
  max_count = 0
  max_word = ""
  string.chars.each_index do |idx1|
    string.chars.each_index do |idx2|
      next if idx1 > idx2
      temp = string[idx1..idx2]

      if palindrome?(temp) && temp.length > max_count
        max_word = temp
        max_count = temp.length
      end
    end
  end
  if max_count > 2
    max_count
  else
    false
  end


end

def palindrome?(str)

  str == str.reverse

end
